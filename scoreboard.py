from turtle import Turtle
FONT = ("Courier", 24, "normal")


class Scoreboard(Turtle):

    def __init__(self):
        super().__init__()
        self.levels = 0
        self.color("black")
        self.penup()
        self.hideturtle()
        self.update_score()

    def update_score(self):
        self.clear()
        self.goto(-260, 260)
        self.write(f'Level: {self.levels}', font=FONT)

    def get_point(self):
        self.levels += 1



