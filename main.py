import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard


FONT = ("Courier", 24, "normal")
screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)

player = Player()
game_over = Player()
game_over.hideturtle()
car_manager = CarManager()
score = Scoreboard()

screen.listen()
screen.onkey(player.up, "Up")


game_is_on = True
while game_is_on:
    time.sleep(0.1)
    screen.update()

    car_manager.create_car()
    car_manager.move_cars()
    if player.is_at_finish_line():
        player.go_to_start()
        car_manager.level_up()
        score.get_point()
        score.update_score()

    for car in car_manager.all_cars[1:]:
        if player.distance(car) < 20:
            game_is_on = False
            game_over.goto(-80, 0)
            game_over.write("GAME OVER", font=FONT)


screen.exitonclick()
